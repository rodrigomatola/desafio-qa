#Funcionalidade 01-Entrar em grupo de Whatsapp por um link 

@tag
Feature: Whatsapp Group Link
	Entrar em um grupo de Whatsapp por um link de convite

@tag1
Scenario: Entrar no grupo de Whatsapp por um link dentro do Whatsapp
	Given um link com o endereco de um grupo do Whatsapp
	When eu toco no link
	Then a janela do grupo se abre
	And a mensagem "Você entrou usando o link de convite deste grupo" aparece na tela
	#outras mensagens aparecerão, mas o objetivo aqui é que voce saiba que entrou no grupo através de um link,...
	#e não adicionado por outra pessoa.
	


@tag2
Scenario: Entrar no grupo de Whatsapp por um link externo ao aplicativo
	Given um link com o endereco de um grupo do Whatsapp
	When eu toco no link
	Then a janela do Whatsapp abre no grupo correspondente
	And a mensagem "Você entrou usando o link de convite deste grupo" aparece na tela