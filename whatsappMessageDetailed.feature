#Funcionalidade 02-Detalhar mensagem enviada

@tag
Feature: Ver detalhes de uma mensagem enviada
	Os detalhes mostram a hora em que a mensagem foi entregue
	e a hora em que foi lida.

@tag1
Scenario: Verificar detalhes de uma mensagem enviada
	Given uma mensagem que você enviou
	When a mensagem é pressionada
	And toca-se o ícone (i) na barra localizada acima
	Then a tela título (Dados da mensagem) é aberta
	And a mensagem selecionada aparece no topo
	And o campo (Lida) aparece com um traço ou a hora em que a mensagem foi lida
	And o campo (Entregue) aparece com um traço ou a hora em que a mensagem foi entregue
